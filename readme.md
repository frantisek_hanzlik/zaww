# The Zaww project

> This project is only intended for the web version of ZAV, and backporting to the C# application is not planned.

The Zaww project is a project that roots for creating a simple-to-use multi-platform hack for the ZAV "teaching writing by all ten" program.

# Disclaimer

This application was not created in the desire to cheat the school, which can, (and is) done in a variety of other ways, such as paying other students to complete the exercises for you;
This application was created because of the utter bugginess of ZAV, which causes many problems such as ZAV telling you completely different numbers than it is using throughout the system, which in turn causes countless headaches for the students having to use it.

